module.exports = function (grunt) {
  grunt.initConfig({
    bower_concat: {
      all: {
        dest: 'tmp/vendors-bower.js',
        exclude: [
          'google-hangouts-api'
        ],
        options: { separator : ';\n' }
      }
    },
    coffee: {
      compile: {
        files: {
          'tmp/coffee.js': ['app/coffeescript/**/*.coffee']
        }
      }
    },
    concat: {
      vendors: {
        src: [
          'bower_components/google-hangouts-api/rpc.js',
          'bower_components/google-hangouts-api/gapi.js',
          'tmp/vendors-bower.js'
        ],
        dest: 'www/javascript/vendors.js'
      },
      main: {
        src: [
          'app/javascript/**/*.js',
          'tmp/coffee.js'
        ],
        dest: 'www/javascript/main.js'
      }
    },
    stylus: {
      all:{
        files: {
          'www/css/main.css': ['app/styles/**/*.styl']
        }
      }
    },
    watch: {
      coffee: {
        files: 'app/coffeescript/**/*.coffee',
        tasks: 'coffee'
      },
      concat: {
        files: '<%= concat.main.src %>',
        tasks: 'concat'
      },
      stylus: {
        files: 'app/styles/**/*.styl',
        tasks: 'stylus'
      }
    }
  });
  grunt.loadNpmTasks('grunt-bower-concat');
  grunt.loadNpmTasks('grunt-contrib-coffee');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-stylus');
  grunt.registerTask('default', ['stylus', 'bower_concat', 'coffee', 'concat']);
  grunt.registerTask('dev', ['stylus', 'bower_concat', 'coffee', 'concat', 'watch']);
}
