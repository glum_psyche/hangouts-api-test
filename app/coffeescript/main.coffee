GAPI_CLIENT_ID = '401376741250-arc6cusknr2vnjifd36ini3v0rjh3l2e.apps.googleusercontent.com'
GAPI_CLIENT_SECRET = 'Vhunj16AONMJBXfPZxZkCB3U'
GAPI_API_KEY = 'AIzaSyAKp-nv0ny7QtTzktU-Qz2lVKXlHlsmqIg'
SCOPES = [
  'https://www.googleapis.com/auth/hangout.telephone'
  'https://www.googleapis.com/auth/plus.me'
]

window.init = ->
  $('#main-id').html '<h1>Hello world!</h1>'
  gadgets.util.registerOnLoadHandler ->
    console.log '!!!!!'
    return
  return

window.onGoogleApiLoaded = ->
  console.log 'Google API loaded!'
  gapi.client.setApiKey GAPI_API_KEY
  window.setTimeout checkAuth, 1
  gapi.hangout.onApiReady.add (eventObj) ->
    console.log 'onApiReady', eventObj
    return
  return

window.authButtonClicked = ->
  checkAuth false
  return

window.checkAuth = (immediate = true) ->
  gapi.auth.authorize
    client_id: GAPI_CLIENT_ID
    scope: SCOPES
    immediate: immediate
  , (authResult) ->
    console.log 'Auth result', authResult
    $googleAuthButton = $('#google-auth-button')
    if authResult? and not authResult.error?
      $googleAuthButton.hide()
      callApi()
    else
      $googleAuthButton.show()
      $googleAuthButton.on 'click', authButtonClicked
    return
  return

window.callApi = ->
  console.log 'Call API'
  console.log 'ID', gapi.hangout.getHangoutId()
  gapi.hangout.onApiReady.add (eventObj) ->
    console.log 'onApiReady', eventObj
    return
  return
