# Google Hangout API Testing

## Prepare

First, is needed to install `npm` module `http-server` globally:

```
$ npm install http-server -g
$ npm install grunt-cli -g
```

Next, install `bower` and `npm` modules

```
$ npm install && bower install
```

## Start

To start server it is needed simply start

```
$ grunt default
$ http-server .www
```

or use this script:

```
$ ./start-server
```

Developer starts locally on `http://localhost:9080`
